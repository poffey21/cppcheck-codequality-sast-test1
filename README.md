# cppcheck-codequality-sast-test1

Verifying that you're able to run `cppcheck` to generate a GitLab SAST Report

## Getting started

Simply use the CI Job definition to run cppcheck. The container image has `cppcheck` installed and a version of the `cppcheck-codequality` installed that allows you to generate a GitLab SAST report (if the output file is called `sast.json`).

```yaml
gitlab_sast_artifact_report_using_container:
  image: registry.gitlab.com/poffey21/cppcheck-codequality-sast-packager/main:latest
  stage: test
  script:
    - cppcheck --xml --enable=warning,style,performance ./tests/cpp_src/ 2> cppcheck.xml
    - cppcheck-codequality -o sast.json
  artifacts:
    when: always
    paths:
      - sast.json
    reports:
      sast: sast.json
```
